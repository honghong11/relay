class CommentsController < ApplicationController
	def create
		@comment = Comment.new(comment_params)
		@comment.save

		redirect_to @comment.story
	end

	private

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:content, :story_id)
    end

end
